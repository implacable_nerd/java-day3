package com.Banking;

public abstract class Account {
	private String accountNumber;
	private double balance;
	
	public Account() {
		super();
	}

	public Account(String accountNumber, double balance) {
		this.accountNumber = accountNumber;
		this.balance = balance;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public abstract String getAccountType();

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "Account \n[accountNumber:\t" + accountNumber + "\n, balance:\t" + balance + "]";
	}
	
}
