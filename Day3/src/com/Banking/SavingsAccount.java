package com.Banking;

public class SavingsAccount extends Account{
	private String accountType;

	public SavingsAccount(String accountNumber, double balance) {
		super(accountNumber, balance);
		this.accountType = "Savings";
	}
	
	public String getAccountType() {
		return this.accountType;
	}
	@Override
	public String toString() {
		return "CurrentAccount [getAccountNumber()=" + super.getAccountNumber() + ", getBalance()=" + super.getBalance()
				+ ", accountType=" + accountType + "]";
	}
	
	
}
