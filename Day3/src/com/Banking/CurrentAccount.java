package com.Banking;

public class CurrentAccount extends Account{
	private String accountType;

	public CurrentAccount(String accountNumber, double balance) {
		super( accountNumber, balance);
		this.accountType = "Current";
	}
	
	public String getAccountType() {
		return this.accountType;
	}
	@Override
	public String toString() {
		return "CurrentAccount [getAccountNumber()=" + super.getAccountNumber() + ", getBalance()=" + super.getBalance()
				+ ", accountType=" + accountType + "]";
	}
	
	
}
