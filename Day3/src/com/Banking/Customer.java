package com.Banking;

public class Customer {
	private String fname;
	private String lname;
	private String customerId;
	private Account account;
	
	public Customer() {
		super();
	}

	public Customer(String fname, String lname, String customerId, Account account) {
		super();
		this.fname = fname;
		this.lname = lname;
		this.customerId = customerId;
		this.account = account;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Override
	public String toString() {
		return "Customer [fname=" + fname + ", lname=" + lname + ", customerId=" + customerId + ", account=" + account
				+ "]";
	}
	
}
